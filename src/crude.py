#!/usr/bin/python3.7

#  Crude [krood]
#   adj:
#       Statistics In an unanalyzed form; not adjusted
#       to allow for related circumstances or data.
#  ------
#  CRUDE is an adaptable tool/philosophy for scraping and analyzing
#  raw text files from any HTML table of links. This version
#  is for demonstration purposes. For now it only scrapes
#  publicly listed links on PasteBin.
#
#  ABOUT: I wrote CRUDE as a first attempt at writing tools
#  for OSINT (Open Source Intelligence). Why pastebin? Pastebin
#  has become the de-facto public clipboard for a variety of
#  applications. New paste services also exist and will be added
#  to this code over time. Each have unique requirements.
#
#  TO PASTEBIN: When will life-time pro accounts be available?
#  Were they disabled to prevent nefarious actors from scraping,
#  and you were forced to adopt a different business model??
#
#  DISCLAIMER: Use at your own risk! It is a violation of
#  Pastebin's ToS to use automated scraping techniques w/o
#  the appropriate permission and API access. Abuse of this
#  tool will result in your IP becoming blocked & possibly
#  black-listed! Don't be a d*ck!

# ------ MODULES ------ #
import os
import sys
import time
import subprocess
import csv
from itertools import cycle
import requests
from lxml.html import fromstring

# ------ ------- ------ #

version = "1.5.1a"

# ------ SPLASH ------ #
def _vis_():
    title_file = open("./include/title.txt", 'r')
    title = title_file.read()
    print(title)
    print("Version: ", version)
    title_file.close()

_vis_()
# ------ ------ ------ #

# ------ QUERY ------ #
print('\033[93mEnter a keyword, then press enter. Return null to complete the list:' )
keywords = []
matchies = []
while 1:
    keyword = input('\033[93m --> ')
    if keyword == '':
        break
    keywords.append(keyword)
print("The following keywords will be searched: ", str(keywords))
# ------ ----- ------ #

# SESSION ID #
# Create an unique ID to head a results catalog
# hash the value of the keywords array
session_name = hash(str(keywords))

# ------ GET PROXIES ------- #

def get_proxies():
    url = 'https://free-proxy-list.net/'
    response = requests.get(url)
    parser = fromstring(response.text)
    proxies = set()
    print('\033[97m[ヮ] Getting Proxylist!')
    for i in parser.xpath('//tbody/tr')[:10]:
        if i.xpath('.//td[7][contains(text(),"yes")]'):
            #Grabbing IP and corresponding PORT
            proxy = ":".join([i.xpath('.//td[1]/text()')[0], i.xpath('.//td[2]/text()')[0]])
            proxies.add(proxy)
    return proxies

# ------ 0000000000 -------- #


# ------ COLLECT ------ #
# This function accomplishes two goals:
# (1) Grab list of links and append them to the CSV
# (2) Download raw text dumps

def _collect_():
    print("\033[92m[+] Dumping Links")
    with open(os.devnull, 'wb') as devnull:
        subprocess.call(["./src/collect.sh"], stdout=devnull, stderr=subprocess.STDOUT)

    print("[+] Filtering Links")
    subprocess.call('./src/filter.sh')
# ------ ------- ------ #

# ------ INDEXER ------ #
# (1) Compare links
# (2) Request Paste
# (3) Create a File

def _store_():
    proxies = get_proxies()
    proxy_pool = cycle(proxies)
    change_ip = True
    link_file = str('./tmp/links.csv')
    # Open file with csv.reader, send all contents to list
    with open(link_file, 'r') as f:
        data = [list(line) for line in csv.reader(f)]
        pasta = 0 # Pastes Counter (Ingestor)

        # Read row x row, check status of entries.
        # "{'/********,[0-3]'}" --> 0 = Unread, 1 = Read, 2 = Collected, 3 = Error(skip)

        for row in data:
           # Compare link status
            status = row[1]

            # Status is unread, download required
            if status == str('0'):

               # Ok, let's append that link to a complete url string.

                link = str(row[0])
                url = "http://www.pastebin.com/raw%s" % (link)
               # Count number of pastes downloaded.
                sys.stdout.flush()

                # REQUEST #
                try:
                    if change_ip == True:
                        proxy = next(proxy_pool)

                    print("\033[92m[+] Ingesting Content [%d]" % pasta, '['+proxy+']', end='\r')
                    sys.stdout.flush()

                    proxies = {"https": proxy, "http": proxy}
                    raw_paste = requests.get(url, proxies=proxies, timeout=15)
                    paste = raw_paste.text

                    # Create File.
                    pasta += 1
                    path = str('./store'+link+'.txt') # Unique filename,  "~/store/********.txt"
                    mode = 'a' if os.path.exists(path) else 'w+'
                    with open(path, mode) as a:
                        a.write(paste)
                        a.close()
                        time.sleep(5) # REQUIRED, pastebin: 'uguu no DoS pls user-san'

                    # Modify link status to indicate "Read"
                    row[1] = str('1')
                    change_ip = False

                # Error Handling #
                except requests.exceptions.Timeout as T:
                    change_ip = True
                except requests.exceptions.RequestException as E:
                    row[1] = str('3')
                    change_ip = True
                    continue

        # Indicate writing changes to DB
        print('\n\033[95m[$] Updating DB')

        # Writing Out DB to new list
        write_data = list(data)
        f.close

    # The links.csv must be re-opeend as write-mode
    with open(link_file, 'w') as f:
        writer = csv.writer(f)
        for row in write_data:
            writer.writerow(row)
        f.close()
# ------ ------- ------ #
# ------ ANALYZER ------- #
# Analyze collected files and return
# filenames that match keywords

def _analyzer_(matchies):

    link_file = str('./tmp/links.csv')

    with open(link_file, 'r') as f:
        data = [list(line) for line in csv.reader(f)]
        chicken = 0 # Match counter
        print("\033[92m[#] Starting Analyzer")

        for row in data:
            status = row[1]
            if status == str('1'):
                file_name = row[0]
                file_path = str('./store'+file_name+'.txt')
                cat= open(file_path, 'r')
                cat_line = cat.readlines()
                for line in cat_line:
                    if any(word in line for word in keywords):
                        chicken += 1
                        matchies.append(file_path)
                        row[1] = str('2')
        print("[#] Matchies [%d]" % chicken)
        #print(matchies)
        return list(set(matchies))
        #write_catalog = list(data)
        #f.close()
        #print("[$] Cataloging")
        #with open(link_file, 'w') as f:
        #    writer = csv.writer(f)
        #    for row in write_catalog:
        #        writer.writerow(row)
        #    f.close()
# ------ -------- ------- #

# ------ CATALOG ------ #
# The catalog function returns a simple text file which:
# (1) Creates a header containing the keywords of the search,
# (2) Appends only new file names to the list.
# (2.a) Technically, this file will be overwritten each cycle.

def _catalog_(session_name):
    # Create an unique file for this session
    path = './store/results-'+str(session_name)
    mode = 'a' if os.path.exists(path) else 'w+'
    with open(path, mode) as results:
        if mode == 'w+':
            results.writelines("Keyword: %s\n" % word for word in keywords)
        results.writelines("%s\n" % match for match in matchies)
        results.close()
    print("\033[95m[$] Updating Catalog")
# ------- ------ ------ #

# ------ LOOP ------ #
try:
    while True:
        _collect_()
        _store_()
        _analyzer_(matchies)
        _catalog_(session_name)
except KeyboardInterrupt:
    print('\n')
    print("\033[91m[!] Process Interrupted! link database not updated!")
# ------ ---- ------ #

# EOF ^,^
