#!/bin/bash

### DEPENDS! Xidel ###

#Filter out non-standard links
#append to links.csv
exec xidel --trace-stack --silent --extract "//a/@href" ./tmp/links.tmp | grep -e "/[a-zA-Z0-9]\{8\}$" | awk '{print $0 ",0"}'  >> ./tmp/links.csv

awk -i inplace '!x[$0]++'  ./tmp/links.csv

## EOF ^.^ ##
