<pre>  
   ██████╗██████╗ ██╗   ██╗██████╗ ███████╗██╗
  ██╔════╝██╔══██╗██║   ██║██╔══██╗██╔════╝██║
  ██║     ██████╔╝██║   ██║██║  ██║█████╗  ██║
  ██║     ██╔══██╗██║   ██║██║  ██║██╔══╝  ╚═╝
  ╚██████╗██║  ██║╚██████╔╝██████╔╝███████╗██╗
   ╚═════╝╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝╚═╝
</pre>

**Crude** [*kr*-ood]
*(adj)*
>        Statistics In an unanalyzed form; not adjusted
>        to allow for related circumstances or data.




**CRUDE!** is an adaptable tool/philosophy for scraping and analyzing raw text files from any HTML table of links. This version is for demonstration purposes. For now it only scrapes publicly listed links on PasteBin. I wrote CRUDE as a first attempt at writing tools for OSINT (Open Source Intelligence). Why pastebin? Pastebin has become the de-facto public clipboard for a variety of applications. New paste services also exist and will be added to this code over time. Each have unique requirements.

**Current version**: 1.5.1a

**DISCLAIMER**: Use at your own risk! Abuse of this tool will potentially blacklist your IP!


# INSTALL 

**Requirements** 

*  Python 3 is required to run CRUDE! If you're interested in this tool, you probably already have it. ;)
*  Xidel - http://www.videlibri.de/xidel.html
*  html-xml-utils - https://www.maketecheasier.com/manipulate-html-and-xml-files-from-commnad-line/

`sudo apt update && sudo apt install xidel html-xml-utils`

**Download**
    `git clone https://gitlab.com/pyrot/crude.git`

**Signatures**

```
crude.py 
    SHA256  396d80c7bf555f56d44811d5b7dd49db583552c1e40d720b68aa333b92a48396
    MD5SUM  45459658a1e116ed1954dcb841d48388
```




**Setup**
```
    cd CRUDE
    sudo chmod 775 start.sh
    cd src
    sudo chmod 775 *.sh
    
```



# USAGE

Start Crude, 
`    ./start.sh`

Crude will ask you for keywords to pass into a list. 
```
    Enter a keyword, then press enter. Return null to complete the list:
     --> password
     --> username
     --> 
```
That's it! 

Now to explain what is happening in the background. 

1.  The very first thing CRUDE will do is request a fresh table of the most recent public links posted. Then this table will be filtered into a list, removing irrelevant links & duplicates. 
2.  A list of https proxies is acquired from [free-proxy-list.net](https://free-proxy-list.net)
3.  CRUDE will then request a raw text page by appending the link to this string '`http://pastebin.com/raw/<LINK>`'
4.  New files will be created for each raw paste and stored within the directory `/store`
5.  After these files are downloaded, the status of each link is modified to represent whether it's been read or if there was an error
6.  Remember those keywords? Each file will then be scanned, in a way very similar to `grep -rn '$keyword' *.txt`. Resulting filenames are then appended to a growing catalog file. Each catalog is session unique. 
7.  Watch your catalog of positives grow: `tail -f ../store/results--<UNIQUE HASH VALUE>`
8.  Profit. 
  

<img src=https://gitlab.com/pyrot/crude/raw/master/screenshot.png>

# ToDo 

Version 2.0
* Modes: pass arguments into initial string
    * Different paste sources, pastebin, paste.ee... etc
    * Analyzer Only 
    * WordList - Import a keyword list. 
    * Run-at-startup - Import keywords, and detach. 
    * Download All or Selectively
* Catalog improvements
    * Sort results by keyword
    * Dedup
* Pastebin Alternatives
* Social media??

